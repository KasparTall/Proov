package ee.bsc.koolitused;

public class Proov9 {
	// leida iga tabeli/ristküliku pindala ja see välja trükkida
	public static void main(String[] args) {
		int[][] ristkylikuMootudeTabel = { { 1, 0 }, { 2, 5 }, { 98, 75 } };

		for (int reaNr = 0; reaNr < ristkylikuMootudeTabel.length; reaNr++) {
			int esimeneKylg = 0;
			int teineKylg = 0;
			for (int veeruNr = 0; veeruNr < ristkylikuMootudeTabel[reaNr].length; veeruNr++) { // iga for-iga defineerid
																								// uuesti
				// muutujad
				if (veeruNr == 0) {
					esimeneKylg = ristkylikuMootudeTabel[reaNr][veeruNr];
				} else {
					teineKylg = ristkylikuMootudeTabel[reaNr][veeruNr];
				}
			}
			System.out.println("Ristkülikul külgedega " + esimeneKylg + " ja " + teineKylg + " on pindalaga"
					+ (esimeneKylg * teineKylg));

		}
		System.out.println("----------------lyhem versioon--------------------");
		for (int reaNr = 0; reaNr < ristkylikuMootudeTabel.length; reaNr++) { // [reaNr][0] sellest reast anna mulle
																				// esimene veerg
			System.out.println("Ristkülik külgedega " + ristkylikuMootudeTabel[reaNr][0] + " ja "
					+ ristkylikuMootudeTabel[reaNr][1] + " on pindala "
					+ (ristkylikuMootudeTabel[reaNr][0] * ristkylikuMootudeTabel[reaNr][1]));

		}
		System.out.println(" ");// tühi rida
		System.out.println("----------------while--------------------");
		int reaNr = 0;
		while (reaNr < ristkylikuMootudeTabel.length) {
			System.out.println("Ristkülik külgedega " + ristkylikuMootudeTabel[reaNr][0] + " ja "
					+ ristkylikuMootudeTabel[reaNr][1] + " on pindala "
					+ (ristkylikuMootudeTabel[reaNr][0] * ristkylikuMootudeTabel[reaNr][1]));
			reaNr++;
		}
		System.out.println("----------------for each--------------------"); // tabeli laps on rida, rea laps on int
		for (int[] yheristkylikuMoodud : ristkylikuMootudeTabel) {
			System.out.println("Ristkülik külgedega " + yheristkylikuMoodud[0] + " ja " + yheristkylikuMoodud[1]
					+ " on pindala " + (yheristkylikuMoodud[0] * yheristkylikuMoodud[1]));

			for (int ristkylikuKylg : yheristkylikuMoodud) { //ei öelda kumba külge võtma peab
				System.out.println("ristküliku külg on " + ristkylikuKylg);
			}

		}
	}
}