package ee.bsc.koolitused;

public class Proov7 {

	public static void main(String[] args) {
		int arv = Integer.parseInt(args[0]);
		if (arv % 2 == 0) {
			System.out.println("Arv " + arv + " on paaris");
		} else {
			System.out.println("Arv " + arv + " on paaritu");
		} // run-runconfiguration-annad arvu argumentides
		System.out.println((arv % 2 == 0) ? "Arv " + arv + " on paaris" : "Arv" + arv + " on paaritu");
	}
}
