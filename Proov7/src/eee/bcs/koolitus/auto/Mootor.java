package eee.bcs.koolitus.auto;

public class Mootor {
	private int voimsus; // see muutuja on klassi tasemel
	private KytuseTyyp kytuseTyyp;

	public int getVoimsus() { // public - meetodi nähtavus on avalik väljaspoole packagei, int-see tyyp mida
								// tagstatakse, getVoimsus-muutuja nimi- booleaniga on ainu8lt erand
		return voimsus;

	}

	public void setVoimsus(int uusVoimsus) { // void-meetodi tagastustüüp, setteril vähemalt üks muutuja, mis sisse
												// läheb, antud juhul int
		this.voimsus = uusVoimsus; // this ütleb, et ta läheb tagasi ülesse, mitte ei arvesta viimast.
	}

	public KytuseTyyp getKytuseTyyp() {
		return kytuseTyyp;
	}

	public void setKytuseTyyp(KytuseTyyp kytuseTyyp) {
		this.kytuseTyyp = kytuseTyyp;
	}

	@Override // ei pea panema
	public String toString() {
		return "Mootor: võimsus=" + voimsus+ "; kytusetypp=" + kytuseTyyp;
	}
}