package eee.bcs.koolitus.auto;

public class Auto {
	private int usteArv;
	private int kohtadeArv;
	private Mootor mootor;
	private int id; // mittestaatiline muutuja, igal ühel oli oma isiklik
	private static int counter = 1; // seda hakkavad kõik autoobjektid jagama

	public Auto() { // see on defolt konstruktor ----nähtavus-tagatus(siin seda ei ole)-meetodi nimi
		id = counter;
		counter++; //ei ole seotud konktreetse autoga
	}

	public Auto(int usteArv, Mootor mootor) { // tegin ise konstruktori
		this();// et saada kolmandale autole
		this.usteArv = usteArv;
		this.mootor = mootor;
	}

	public int arvutaKindlustus(String kasutajaNimi, int periood) {
		if (periood <= 0 || kasutajaNimi.equals("")) {
			return 0;
		}
		int summa = arvutaKindlustus(periood) + 10;
		if (kasutajaNimi.equals("Mari")) {
			summa += 15; // summa vana väärtus pluss 15
		} else if (kasutajaNimi.equals("Mati")) {
			summa += 45;
		}
		return summa;

	}

	public int arvutaKindlustus(int vanus, int periood) {
		int summma = arvutaKindlustus(periood);
		if (vanus < 26) {
			summma *= 2;
		}
		return summma; // veel saab lühemaks kui kasutada siin tingimuslauset
	}

	public int arvutaKindlustus(int periood) { // testitav lõik
		if (periood <= 0) {
			return 0;
		}
		return periood * 10;

		// variant2
		// if (periood <=10) {
		// return 0;
		// }else{
		// return periood*10;
		// }
	}// setter getter saime parema klahviga, siis source ja generate setter ja getter

	public int getId() {
		return id;
	}

	public static int getCounter() {
		return counter;

	}

	public int getUsteArv() {
		return usteArv;
	}

	public Auto setUsteArv(int usteArv) { // Voidi asemel kirjutasime Auto ja tegime juurde rea return this;
											// Konstruktoreid on vaja et sellest klassist objekte luua
		this.usteArv = usteArv;
		return this; // return this peab olmea viimane rida, siis töötam ainult škoda autoga
	}

	public int getKohtadeArv() {
		return kohtadeArv;
	}

	public Auto setKohtadeArv(int kohtadeArv) {
		this.kohtadeArv = kohtadeArv;
		return this;
	}

	public Mootor getMootor() {
		return mootor;
	}

	public Auto setMootor(Mootor mootor) {
		this.mootor = mootor;
		return this;

	}

	@Override
	public String toString() {
		return "Auto[ustearv=" + "; kohtadeArv=" + kohtadeArv + ", mootor=" + mootor + "]";
	}
}