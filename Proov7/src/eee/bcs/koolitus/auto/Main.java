package eee.bcs.koolitus.auto;

public class Main {
	public static int factorialCalculation(int givenNumber) {
		int factorial = givenNumber;
		if (givenNumber <= 1) {
			return factorial;

		} else {
			return factorialCalculation(givenNumber - 1) * givenNumber;
		}
	}

	public static String leiaSumma(String miskiTekst, int... arvud) { // sulgudes on x- arv intte, 
		//eelmine variant oli public static int leiaSumma(String miskiTekst, int... arvud)
		int summa = 0;
		for (int arv : arvud) {
			summa += arv;
		}
		return miskiTekst + summa; //eelmine varinat oli return summa

	}

	public static void main(String[] args) {

		System.out.println(leiaSumma("fedaedf ", 1, 5, 8));

		System.out.println(factorialCalculation(5));

		Mootor mootor1 = new Mootor();
		mootor1.setVoimsus(84);
		mootor1.setKytuseTyyp(KytuseTyyp.BENSIIN);
		System.out.println(
				"Mootoril on võimsus " + mootor1.getVoimsus() + "KW, kytusetyyp on " + mootor1.getKytuseTyyp()); // siin
																													// to.Stringi
																													// ei
																													// kasutatud
		System.out.println("Mootor < " + mootor1); // siin kasutatakse to.Stringi, kombilaator paneb selle ise

		Auto autoVolvo = new Auto();// loome uue auto
		autoVolvo.setKohtadeArv(5);
		autoVolvo.setMootor(mootor1); // kasutame sama mootorit
		autoVolvo.setUsteArv(5);

		System.out.println("esimese auto id= " + autoVolvo.getId()+ " ja counter= " + Auto.getCounter());
		System.out.println(autoVolvo);
		System.out.println(autoVolvo.getMootor().getKytuseTyyp());// autoVolvo.getMootor()- annab mulle mootori

		Mootor mootor2 = new Mootor(); // teeme uue mootori
		mootor2.setVoimsus(125);
		mootor2.setKytuseTyyp(KytuseTyyp.DIISEL);

		Auto autoŠkoda = new Auto(); // kui võtad semikooloneid ära siis saad lühemalt kirjutada
		autoŠkoda.setKohtadeArv(2);
		autoŠkoda.setUsteArv(3);
		autoŠkoda.setMootor(mootor2);
		System.out.println(autoŠkoda);
		
		System.out.println("Teise auto id= " + autoŠkoda.getId()+ " ja counter= " + Auto.getCounter());

		Mootor mootor3 = new Mootor(); // teeme uue mootori
		mootor3.setVoimsus(64);
		mootor3.setKytuseTyyp(KytuseTyyp.BENSIIN);

		Auto autoYaris = new Auto(5, mootor3).setKohtadeArv(5); // loome auto ilma defolt konstruktorita
		System.out.println(autoYaris);
		
		
		
		System.out.println("esimese auto id= " + autoVolvo.getId()+ " ja counter= " + Auto.getCounter());
		System.out.println("Teise auto id= " + autoŠkoda.getId()+ " ja counter= " + Auto.getCounter());
		System.out.println("Kolmanda auto id= " + autoYaris.getId()+ " ja counter= " + Auto.getCounter()); //counterid tulevad kõigil ühesugune, id on kõigil erinev
		

		System.out.println("vanuse järgi:" + autoYaris.arvutaKindlustus(28, 10));
		System.out.println("Nime järgi: " + autoYaris.arvutaKindlustus("Mati", 10));
		// System.out.println("Nime järgi: " + autoYaris.arvutaKindlustus(10, "Mati"));
		// ei anna tulemust järjekord on erinev ja me ei ole sellist loonud

	}

	private static char[] LeiaSumma(int i, int j, int k) {
		// TODO Auto-generated method stub
		return null;
	}
}
