package ee.bcs.koolitused.employee;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Main {
	public static void changeSalary(Employee employee) {// BigDecimal on täpsem andmetyyp kui Double
		// employee = new Employee();

		// employee.setSalary(employee.getSalary().add(BigDecimal.valueOf(100)));
		changeSalary(employee, BigDecimal.valueOf(100));
	}

	public static void changeSalary(Employee employee, BigDecimal salaryChange) {
		employee.setSalary(employee.getSalary().add(salaryChange)); // BigDeciamlis ei saa teistmoodi liita kui kasutada
																	// .add, punkt peab alati ees olema
	} // see on tehe

	public static void main(String[] args) throws ParseException {
		Employee employee = new Employee();
		employee.setSalary(BigDecimal.valueOf(1_000));
		changeSalary(employee);
		System.out.println(employee.getSalary());
		changeSalary(employee, BigDecimal.TEN); // üks kümme ja null saad välja kirjutada ainsatena kuna nad on
												// levinumad konstandid
		System.out.println(employee.getSalary());

		Employee employeeWiththeBigConstructor = new Employee("Mari", "", "Kask",
				new SimpleDateFormat("dd-MM-yyyy").parse("31-08-1992"),
				new SimpleDateFormat("dd-MM-yyyy").parse("31-08-2015"), null);

		Employee employeeWithBuilder = new Employee.EmployeeBuilder().firstName("Mati").lastName("Tamm")
				.birthDate(new SimpleDateFormat("dd-MM-yyyy").parse("05-05-1982"))
				.startingDate(new SimpleDateFormat("dd-MM-yyyy").parse("31-08-2000")).build();
		
		
		System.out.println( new SimpleDateFormat("dd-MM-yyy").format(employeeWithBuilder.getBirthDate()));
	}

}
