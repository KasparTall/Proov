package ee.bcs.koolitus.employeemanagement.dao;

import java.util.Date;

public class Human {

	private int id;
	private String personalIdCode;
	private String firstname;
	private String lastname;
	private Date birthday;
	private char gender;
	private int addressId;

	public int getId() {
		return id;
	}

	public Human setId(int id) {
		this.id = id;
		return this;
	}

	public String getPersonal_id_code() {
		return personalIdCode;
	}

	public Human setPersonal_id_code(String personalIdCode) {
		this.personalIdCode = personalIdCode;
		return this;
	}

	public String getFirstname() {
		return firstname;
	}

	public Human setFirstname(String firstname) {
		this.firstname = firstname;
		return this;
	}

	public String getLastname() {
		return lastname;
	}

	public Human setLastname(String lastname) {
		this.lastname = lastname;
		return this;
	}

	public Date getBirthday() {
		return birthday;
	}

	public Human setBirthday(Date birthday) {
		this.birthday = birthday;
		return this;
	}

	public char getGender() {
		return gender;
	}

	public Human setGender(char gender) {
		this.gender = gender;
		return this;
	}

	public int getAddress_id() {
		return addressId;
	}

	public Human setAddress_id(int addressId) {
		this.addressId = addressId;
		return this;
	}

	@Override
	public String toString() {
		return "Human[id=" + id + ", personalCode= " + personalIdCode + ", firstname=" + firstname + ", lastname="
				+ lastname + ", birthday=" + birthday + ", gender=" + gender + ", addressId=" + addressId + "]";
	}
}
