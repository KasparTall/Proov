package ee.bcs.koolitus.liikumisvahendid;

public class Mootorsoiduk extends Liikumisvahend {

	private String regNr;
	private JuhiloaKategooria juhiloakategooria;

	public String getRegNr() {
		return regNr;
	}

	public Mootorsoiduk setRegNr(String regNr) {
		this.regNr = regNr;
		return this;
	}

	public JuhiloaKategooria getJuhiloakategooria() {
		return juhiloakategooria;
	}

	public Mootorsoiduk setJuhiloakategooria(JuhiloaKategooria juhiloakategooria) {
		this.juhiloakategooria = juhiloakategooria;
		return this;
	}

}
