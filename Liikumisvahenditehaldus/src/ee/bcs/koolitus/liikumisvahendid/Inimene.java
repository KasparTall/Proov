package ee.bcs.koolitus.liikumisvahendid;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ee.bcs.koolitus.inimene.Inimene;

public class Inimene {

	private String eesNimi;
	private String pereNimi;
	private Date synniKuupaev;
	private JuhiloaKategooria juhiloaKategooria;
	
	public String getEesNimi() {
		return eesNimi;
	}

	public Inimene setEesNimi(String eesNimi) {
		this.eesNimi = eesNimi;
		return this;
	}

	public String getPereNimi() {
		return pereNimi;
	}

	public Inimene setPereNimi(String pereNimi) {
		this.pereNimi = pereNimi;
		return this;
	}

	public Date getSynniKuupaev() {
		return synniKuupaev;
	}

	public Inimene setSynniKuupaev(Date synniKuupaev) {
		this.synniKuupaev = synniKuupaev;
		return this;
	}

	public JuhiloaKategooria getJuhiloaKategooria() {
		return juhiloaKategooria;
	}

	public Inimene setJuhiloaKategooria(JuhiloaKategooria juhiloaKategooria) {
		this.juhiloaKategooria = juhiloaKategooria;
		return this;
	}
		List<JuhiloaKategooria> kategooria = new ArrayList<>();
		
	

}
