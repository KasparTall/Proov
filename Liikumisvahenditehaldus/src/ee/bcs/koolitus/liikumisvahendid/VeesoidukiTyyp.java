package ee.bcs.koolitus.liikumisvahendid;

public enum VeesoidukiTyyp {

	Paat, Laev, Jaht, Purjekas
}
