package ee.bcs.koolitus.liikumisvahendid;

public class Auto extends Mootorsoiduk {

	private AutoTyyp autoTyyp;
	
	public Auto(Omanik omanik, String regNr, JuhiloaKategooria noutudKategooria) {
		super(omanik, regNr, noutudKategooria);
	}

	public AutoTyyp getAutoTyyp() {
		return autoTyyp;
	}

	public Auto setAutoTyyp(AutoTyyp autoTyyp) {
		this.autoTyyp = autoTyyp;
		return this;
	}
	@Override
	public String toString() {
		return "Auto [autoTyyp=" + autoTyyp + ", regNumber=" + getRegNumber() + ", noutudJuhiloaKategooria()="
				+ getNoutudJuhiloaKategooria() + ", id()=" + getId() + ", omanikud()=" + getOmanikud() + "]";

}
}