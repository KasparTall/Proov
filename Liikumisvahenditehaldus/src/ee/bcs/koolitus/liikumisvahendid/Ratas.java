package ee.bcs.koolitus.liikumisvahendid;

public class Ratas extends Liikumisvahend {

	private int kalkudeArv;
	private String seeriaNr;
	private Istumisasend istumisAsend;
	public int getKalkudeArv() {
		return kalkudeArv;
	}
	public Ratas setKalkudeArv(int kalkudeArv) {
		this.kalkudeArv = kalkudeArv;
		return this;
	}
	public String getSeeriaNr() {
		return seeriaNr;
	}
	public Ratas setSeeriaNr(String seeriaNr) {
		this.seeriaNr = seeriaNr;
		return this;
	}
	public Istumisasend getIstumisAsend() {
		return istumisAsend;
	}
	public Ratas setIstumisAsend(Istumisasend istumisAsend) {
		this.istumisAsend = istumisAsend;
		return this;
	}

}
