package ee.bcs.koolitus.main;

import java.util.ArrayList;
import java.util.List;

import ee.bcs.koolitus.inimene.Inimene;
import ee.bcs.koolitus.inimene.Opetaja;
import ee.bcs.koolitus.inimene.Opilane;

public class Main { // alati suure tähega

	public static void main(String[] args) {
		Inimene kati = new Inimene("Kati Kaasik", 25);
		System.out.println(kati);

		Opilane opilaneMati = (Opilane) new Opilane("Mati Tamm", 7) // siin teeme kaastimisega
				.setKlass(1); // set klass annab tagasi õpilase, set nimi annab tagasi inimese
		// kui oleks void, siis oleks iga set omal real ja ei oleks nii keeruline
		System.out.println(opilaneMati);

		Opilane opilaneMari = new Opilane("Mari Maasikas", 10, 4);
		System.out.println(opilaneMari);

		Opetaja opetajaJyri = new Opetaja("Jüri Juurikas", 38);
		System.out.println(opetajaJyri);
		opetajaJyri.addNewAine("Matemaatika").addNewAine("Muusikaajalugu");
		System.out.println(opetajaJyri);

		opetajaJyri.getAined().add("Ajalugu");
		System.out.println(opetajaJyri);

		Inimene inimene = new Opetaja("Kaarel Kuusk", 40); // kuna õpetaja on inimene //kui panna get ained, siis aineid
															// ei ole
		((Opetaja) inimene).getAined();

		List<Inimene> inimesed = new ArrayList<>();
		inimesed.add(inimene);
		inimesed.add(opetajaJyri);
		inimesed.add(opilaneMari);
	}
}
