package ee.bcs.koolitus.inimene;

public class Inimene { // alati suure tähega
	private String nimi;
	private int vanus;
	
	public Inimene(String nimi, int vanus) {
		this.nimi=nimi; //this.nimi = Minu nimi
		this.vanus=vanus;
	}

	public String getNimi() {
		return nimi;
	}

	public Inimene setNimi(String nimi) {
		this.nimi = nimi;
		return this;
	}

	public int getVanus() {
		return vanus;
	}

	public Inimene setVanus(int vanus) {
		this.vanus = vanus;
		return this;
	}

	@Override
	public String toString() {
		return "inimene [nimi=" + nimi + ", vanus=" + vanus + "]";
	}
}
