package ee.bcs.koolitus.inimene;

public class Opilane extends Inimene { //laiendame inimese klassi

	private int klass;

	//public Opilane() {
		//super(null, 0); //stringile saad anda väärtuse null ja indile paned 0, ära kasuta
		
	//}
	
	public Opilane(String nimi, int vanus) { //konstruktor 1 (konstruktori valid selle järgi, mis andmed on sul olemas on)
		super(nimi, vanus);
	}
	public Opilane(String nimi, int vanus, int klass) { //kontruktor 2
		super(nimi, vanus); // super on inimese klassi käsk (kasutad iis kui on vaja suhelda vanemklassidega), kui siia this panna siis see paneb ülemise kontreuktori käima, mis omakorda paneb super konstruktori käima(üleval)
		this.klass=klass;
	}
	
	public int getKlass() {
		return klass;
	}

	public Opilane setKlass(int klass) {
		this.klass = klass;
		super.toString();
		return this;
	}
@Override
public String toString() {
	//return super.toString()+". õpilane [klass=" + klass+"]";
	return "Õpilane[nimi=" + getNimi() + ", vanus= "+ getVanus() + ", klass=" + klass+"]";
}

}
