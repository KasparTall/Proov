package ee.bcs.koolitus.exceptionhandling;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Main {

	public static void main(String[] args) {
		try (FileWriter writer = new FileWriter(new File("outputResult.txt"));) { // see osa, mida üritatakse käivitada
			// tekstifaili said refreshi alt, kui projekti peal teed parema klahviga
			int i = 1;
			while (i < 5) {
				writer.append("There are " + i + " eggs in the bird nest\n");
				i++;
			}
			writer.append("Chicks have hatched, there is no eggs anymore");
			writer.flush();
			// writer.close(); // File Writer, File Reader, ... tuleb alati kinni panna
			// writer.close() saad ära jätta, kui try otsa paned sulud ja cut-id FileWriter
			// writer = new F..............................)); mis oli enne try all
		} catch (IOException e) { // catche võib olla mitu
			e.printStackTrace();
			System.out.println("There is an error on writing to file outputResult.txt: " + e.getMessage());
		}
		try {
			Human human1 = new Human().setName("ge");// delegeerimiskoht
			System.out.println("human name is " + human1.getName());
		} catch (NameTooShortException e) {
			System.out.println("Exception occured on setting human name: " + e.getMessage());
		}

	}

}
