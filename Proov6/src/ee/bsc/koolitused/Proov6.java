package ee.bsc.koolitused;

public class Proov6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a = 7;
		int b = -5;

		if (a >= 0 && b >= 0) {
			int vastus = a * b;
			System.out.println(" a * b = " + vastus);
			if (a > 6 && b < 6) {
				System.out.print("ahaa");
			}
		} else if (a == 5) {
			System.out.println("ei ole vaja korrutada");
		} else {
			System.out.println("ei olnud sobivat tingimust");
		}
		String antudNimi = "Mari";
		final String MINUNIMI = "Kaspar";
		boolean onMinuNimi = (antudNimi.equals(MINUNIMI)) ? true : false;

		System.out.println("Kas minu nimi on " + antudNimi + "? Vastus: " + onMinuNimi);

		boolean isDoorOpen = true; // kui paned false, siis uks on kinni ja Stringi rida pole üldse vaja
		String uks = (isDoorOpen) ? "Uks on lahti" : "Uks on kinni";
		// võid panna ka String uks = (isDoorOpen == true)
		System.out.println(uks);

		//int v = 1;
		//if (v =v+1) {
			//System.out.println("arv on paaris");
		//} else {
			//System.out.println("arv ei ole paaris");
		//}

	
	}
}
