package ee.bsc.koolitused;

import java.util.Arrays;

public class Tabeliproov {

	public static void main(String[] args) {
		String[][] linnadeMaakonnad = { { "Tallinn", "Harjumaa" }, { "Pärnu", "Pärnumaa" },
				{ "Kuressaare", "Saaremaa" } };
		System.out.println(linnadeMaakonnad);

		for (int reaNr = 0; reaNr < linnadeMaakonnad.length; reaNr++) {
			for (int veeruNr = 0; veeruNr < linnadeMaakonnad[reaNr].length; veeruNr++) {
				System.out.print(linnadeMaakonnad[reaNr][veeruNr] + " ");

			}
			System.out.println();

		}
		System.out.println("----------------------------");
		System.out.println(Arrays.deepToString(linnadeMaakonnad));
	}

}
