package ee.bsc.koolitused;

public class ExWithSwitch {
	public static void main(String[] args) {
		String lightColor = "Green";

		switch (lightColor.toLowerCase()) //kui .toLowerCase() ei ole siis ta ei arvesta suuri algustähti
		{
		case "green":
			System.out.println("Driver can drive a car.");
			break; //kui break pole siis läheb yellow case 
		case "yellow":
			System.out.println("Driver has to be ready to stop the car or to start driving.");
			break;
		case "red":
			System.out.println("Driver has to be ready to stop the car and wait for graan light.");
			break;
		default:
			System.out.println("You should not drive in disco bar!");
		}
	}
}