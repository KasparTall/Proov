CREATE DATABASE employee_management; 

CREATE TABLE human ( #Varhcar on string
	id INT(11),
	personal_id_code VARCHAR(11),
	eesnimi VARCHAR (25),
	lastname VARCHAR (25),
    birthday DATE,
    gender VARCHAR(1)
);

INSERT INTO address (county, village, farm_name) VALUES ('Harjumaa', 'Madise', 'Karu talu');

-- SELECT * FROM address -- * tähendab annab sulle kõik veerud, selles tabelis
-- WHERE city IS NOT NULL -- tulevad ainult need read kus linn on täidetud
-- AND street LIKE ('%r%') -- %lõpus tähendab, et sa ei tea mis tuleb peale seda, % alguses tähendab, et sa ei tea mis sellele eelneb
SELECT 
    county,
    city,
    CONCAT(street, ' ', house_no, '-', flat_no) AS street_address
FROM
    address
WHERE
    city IS NOT NULL
        AND street LIKE ('Tuukri');
        
        SELECT 
    CONCAT(h.eesnimi, ' ', h.lastname) As nimi, -- kui as nimi ära kustutad siis on veeru nimi kole
    a.county,
    a.farm_name
FROM human h INNER JOIN address AS a ON h.address_id = a.id
WHERE h.eesnimi = 'Mati';


UPDATE human SET lastname = 'Kuusk2', eesnimi = 'Mari2' WHERE  id = 3; -- rea number, milles ta nime ära muudab


DELETE FROM address WHERE id=2;