package ee.bcs.koolitus;

import java.awt.Color;
import java.util.Objects;

public class Auto implements Comparable<Auto> { //(tööriistakast) hakkan siin klassis implementeerima Comparable nimelist liidest.
	private static final int VORDNE = 0;
	private String regNr;
	private Color color;
	private String vinCode;
	private int usteArv;

	public Auto() {
	}

	public Auto(String regNr, Color color, String vinCode, int usteArv) {
		this.regNr = regNr;
		this.color = color;
		this.vinCode = vinCode;
		this.usteArv = usteArv;

	}

	@Override
	public boolean equals(Object obj) { // meetodile läheb sisendiks objekt, mitte auto
		if (this == obj) { // seda saab source kaudu genereerida kõik
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) { // küsime kas ta on äkki auto klassis
			return false;
		}

		final Auto other = (Auto) obj; // teeme kaastimise, ütleme , et see uus obj on Auto, enne tegime ainult
										// abstrakse objekti (rida 23)
		boolean variablesEqual = this.regNr.equals(other.regNr) && this.color.equals(other.color)
				&& this.vinCode.equals(other.vinCode) && this.usteArv == other.usteArv; // kui vinCode on võrdne siis
																						// ongi autod ühesugused

		return variablesEqual;
	}

	// hashCode-i saab source kaudu genereerida kõik
	@Override // proovin hashCode-i, suvaliste numbritega (algarvud)
	public int hashCode() {
		int hash = 5;
		hash = 39 * hash + Objects.hash(regNr); // teine variant ... + regNr.hashCode();
		hash = 39 * hash + Objects.hash(color);
		hash = 39 * hash + Objects.hash(vinCode);
		hash = 39 * hash + usteArv; // kuna tegu on int
		// teine variant hash =39 hash + Integer.valueOf(usteArv).hashCode();
		return hash;

	}

	public String getRegNr() {
		return regNr;
	}

	public Auto setRegNr(String regNr) {
		this.regNr = regNr;
		return this;
	}

	public Color getColor() {
		return color;
	}

	public Auto setColor(Color color) {
		this.color = color;
		return this;
	}

	public String getVinCode() {
		return vinCode;
	}

	public Auto setVinCode(String vinCode) {
		this.vinCode = vinCode;
		return this;
	}

	public int getUsteArv() {
		return usteArv;
	}

	public Auto setUsteArv(int usteArv) {
		this.usteArv = usteArv;
		return this;
	}

	@Override
	public String toString() {
		return "Auto [regNr= " + regNr + ", color= " + color + ", vinCode= " + vinCode + ",  usteArv= " + usteArv + "]";
	}

	@Override // variant, kus võtad selle numbri ja kasutad seda võrdlemiseks
	public int compareTo(Auto otherCar) {
		final int VORDNE = 0;
		final int VAIKSEM = -1;
		final int SUUREM = 1;
		if (this.equals(otherCar)) {
			return 0;
		} else if (this.vinCode.compareTo(otherCar.vinCode) == VORDNE) {
			if (this.regNr.compareTo(otherCar.regNr) == VORDNE) {
				if (this.color.toString().compareTo(otherCar.color.toString()) == VORDNE) {
					return (this.usteArv < otherCar.usteArv) ? -1
							: ((this.usteArv > otherCar.usteArv) ? SUUREM : VORDNE);

				} else {
					return this.color.toString().compareTo(otherCar.color.toString());
				}
			} else {
				return this.regNr.compareTo(otherCar.regNr);
			}

		} else {
			return this.vinCode.compareTo(otherCar.vinCode); // compareTo annab alati int vastuseks
		}
	}
}
