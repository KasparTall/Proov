package ee.bcs.koolitus.main;

import java.awt.Color;
import java.util.Set;
import java.util.TreeSet;

import ee.bcs.koolitus.Auto;

public class Main {

	public static void main(String[] args) { // static on main klassis alati sees
		Set<Auto> allCars = new TreeSet<>();
		Auto volvo = new Auto("123ABC", Color.black, "dslkfa", 5);
		Auto volvo2 = new Auto("123ABC", Color.black, "dslkfa", 5);
		Auto audi = new Auto("567ABC", Color.red, "dslkfa", 5);
		Auto bmw = new Auto("890ABC", Color.blue, "dslkfa", 3);
		allCars.add(bmw);
		allCars.add(volvo2);
		allCars.add(volvo);
		allCars.add(audi);

		System.out.println("volvo=volvo2?- " + volvo.equals(volvo2) + ", volvo hashCode on " + volvo.hashCode()
				+ ", Volvo2 hashCode on " + volvo2.hashCode());
		System.out.println("volvo=audi?- " + volvo.equals(audi));
		System.out.println("bmw=volvo2?- " + bmw.equals(volvo2));
		System.out.println("audi=bmw?- " + audi.equals(bmw));

		for (Auto auto : allCars) {
			System.out.println(auto);
		}
	
	System.out.println(volvo.compareTo(audi));
	}

}
