package ee.bcs.koolitused;

public class Proov2 {
	public static void main(String[] args) {
	
		String stringToFormat ="%s. %s is %s years old. %<s years old is %2$s in %s."	;
		System.out.println(String.format(stringToFormat, 1, "Mari", 29, "Tallinn"));
		
		String textContainingListOfNames = " This is a list of city name: Tallinn, Tartu, Pärnu, Kuressaare";
		String textWithNamesOnly = textContainingListOfNames.split(":")[1];
		String[] arrayOfCityNames = textWithNamesOnly.split(",");
		for (String cityName : arrayOfCityNames) {
			System.out.println(cityName);
	}

}
}

