package ee.bcs.koolitus;

public class DataTypesIntroduction {

	public static void main(String[] args) {
		byte esimeneByte = 127;
		byte teineByte = 16;
		
		Integer test =10;
		
		double esimeneDouble = 5;
		
		System.out.println("esimene byte = "+(esimeneByte + teineByte + test));
		System.out.println("double väärtus on = " + (esimeneDouble + test));
		
		/**
	     byte summa = (byte)(esimeneByte + teineByte);
	    System.out.println("summa on = " + summa); 
		*/
		char esimeneChar = 'a';
		char teineChar = 'h';
		
		System.out.println(esimeneChar);
		System.out.println(teineChar);
		System.out.println(esimeneChar+teineChar);
		// kui paned System.out.println("" + esimeneChar+teineChar); siis saad "ah", kuna see on tühi string
		System.out.println(esimeneChar == 'a');
		System.out.println(esimeneChar == 'k');
		
		System.out.println(test == 12);
		System.out.println(test == 10);
		
		System.out.println(esimeneChar == teineChar);
		char kolmasChar = 'a';
		System.out.println(esimeneChar == kolmasChar);
		
		int counter = 5;
		counter = counter + 1;
		System.out.println("counter = " + (counter=counter + 1));
		counter++;
		System.out.println("counter = " + (++counter));
		System.out.println(counter);
		
		int a = 5;
		int b = ++a;
		System.out.println("a = " + a +  "; b = " + b);
		b = a++;
		System.out.println("a = " + a +  "; b = " + b);
		
		a = 100;
		a += 5;
		System.out.println("a = " + a);
		a =a + 5;
		System.out.println("a = " + a);
		//võtab kogusaeg viimase väärtuse, kui viimasele a-le omistada uus väärtus, siis ta arvestab seda viimast väärtust
		
		//System.out.println(a%2);
		//System.out.println(a%10);
		//System.out.println(a%3);
		
		a=1;
		b=1;
		int c=3;
		System.out.println("a = " + a);
		System.out.println("b = " + b);
		System.out.println("c = " + c);
		System.out.println(a == b);
		System.out.println(a == c);
		a=c;
		//System.out.println("a=c");
		
		int x1=10;
		int x2=20;
		int y1=++x1;
		int y2=x2++;
		System.out.println("x1 = " + x1);
		System.out.println("x2 = " + x2);
		System.out.println("y1 = " + ++x1);
		System.out.println("y2 = " + x2++);
		
		int a1= 18%3;
		System.out.println(a1);
		//System.out.println(b2%10);
		//System.out.println(c%3);
		//System.out.println(d%3);
		
		String esimeneLause = "sdfbgfbsf";
		System.out.println(esimeneLause);
		
		esimeneLause= esimeneLause+", sdffbvsfv";
		System.out.println(esimeneLause);
		
		String eesnimi = "Henn";
		String perenimi = "Sarv";
		
		String koguNimi = eesnimi +" "+ perenimi;
		System.out.println(koguNimi);
		System.out.println("Henn Sarv" == koguNimi);
		System.out.println("Henn Sarv".equals(koguNimi));
		
		String koheKoguNimi = "Henn Sarv";
		System.out.println(koheKoguNimi == koguNimi);
		System.out.println(koheKoguNimi.equals(koguNimi));
		
	   String kymme ="10";
	   int arvKymme= Integer.parseInt(kymme);
	   System.out.println(arvKymme);
	   
	   String kymme2="20";
	   double doubleArv=Double.parseDouble(kymme2);
	   System.out.println(doubleArv);
	   String textDoubleist = ((Double)doubleArv).toString();
	   textDoubleist = "" + doubleArv;
	   
	}

}
