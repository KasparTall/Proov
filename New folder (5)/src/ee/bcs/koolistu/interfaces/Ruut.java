package ee.bcs.koolistu.interfaces;

public class Ruut implements Kujund {
	private double kyljePikkus;

	@Override
	public double arvutaPindala() {
		return Math.pow(kyljePikkus, 2);    //võid ka teha küljepikkus korda küljepikkus
	}

	public double getKyljePikkus() {
		return kyljePikkus;
	}

	public Ruut setKyljePikkus(double kyljePikkus) {
		this.kyljePikkus = kyljePikkus;
		return this;
	}

}
