package ee.bcs.koolistu.interfaces;

public class Kolmnurk implements Kujund {
private double korgus;
private double alus;

@Override
public double arvutaPindala() {
	return (korgus*alus)/2;
}
public double getKorgus() {
	return korgus;
}
public Kolmnurk setKorgus(double korgus) {
	this.korgus = korgus;
	return this;
}
public double getAlus() {
	return alus;
}
public Kolmnurk setAlus(double alus) {
	this.alus = alus;
	return this;
}


}
