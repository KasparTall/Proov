package ee.bcs.koolitus.main;

import ee.bcs.koolistu.interfaces.Kolmnurk;
import ee.bcs.koolistu.interfaces.Kujund;
import ee.bcs.koolistu.interfaces.Ruut;

public class Main {

	public static void main(String[] args) {
		
Kujund ruut = new Ruut().setKyljePikkus(5);
System.out.println(ruut.arvutaPindala());

Kujund kolmnurk = new Kolmnurk().setKorgus(3).setAlus(2);
System.out.println(kolmnurk.arvutaPindala());
	}

}
