package ee.bcs.koolitused;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
//kasutame java klasse

public class Proov4 {

	public static void main(String[] args) {
		String scanneriTekst = "Name:Heleen Maibak Name:Henn Sarv Name:Mari Maasikas";
		Scanner scanner = new Scanner(scanneriTekst);
		while (scanner.hasNext()) {
			System.out.println(scanner.next());
		}
		System.out.println("------------------------------");
		scanner.close();

		Scanner scannerByWord = new Scanner(scanneriTekst);
		scannerByWord.useDelimiter("Name:");
		while (scannerByWord.hasNext()) {
			System.out.println(scannerByWord.next());
		}
		System.out.println("------------------------------");
		scannerByWord.close();
		try (Scanner scannerFile = new Scanner(new File("testFileForScanner.txt")).useDelimiter("Name:")) {
			while (scannerFile.hasNext()) {
				System.out.println(scannerFile.next());
			}
		} catch (FileNotFoundException ex) {
			Logger.getLogger(Proov4.class.getName()).log(Level.SEVERE, null, ex);

		}

	}

}
