package ee.bcs.koolitused;

public class StringBuilderDemo {

	public static void main(String[] args) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("This is first sentence. \n");
      //\n on uus rida
      stringBuilder.append("This is second sentnece added with append. \n");
      int thirdBeginning = stringBuilder.length();
      stringBuilder.insert(thirdBeginning, 
		"This is third sentence added by using insert, is added to the end of string buffer. \n");
      String neljasLause = "This is fourth sentence that goes before third sentence as using same position thirdBeginning. \n";
      stringBuilder.insert(thirdBeginning, neljasLause);
      System.out.println(stringBuilder);

      stringBuilder.delete(thirdBeginning, thirdBeginning + neljasLause.length());
      //et ei peaks pikka lauset kirjutama, siis panin naad muutujasse- nt neljasLause.lenght(). 
      System.out.println(stringBuilder);
	}

}
