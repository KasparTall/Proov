package ee.bcs.koolitused;

import java.util.HashMap; //importisid
import java.util.Map;//importisid

public class Proov13 {

	public static void main(String[] args) {
		Map<String, String> inimesed = new HashMap<>(); // esimene string on key teine on value
		inimesed.put("12345678912", "Mati Karu");
		inimesed.put("12345678913", "Kati Karu");
		inimesed.put("12345678928", "Mummi Taru");
		System.out.println(inimesed); // küsima peab võtme abil mapist, et teada saada 1 unikaalne väärtus

		System.out.println("kellel on isikukood 12345678928? " + inimesed.get("12345678928"));

		for (String key : inimesed.keySet()) {
			if (inimesed.get(key).equals("Mummi Taru")) {
				System.out.println("Mummi Taru isikukood on " + key);
			}

		} // kusutamiseks on vaja remove teha key-le.
		System.out.println("Kas Mummi Taru nimeline inimene on loetelus? " + inimesed.containsValue("Mummi Taru"));
	}
}
