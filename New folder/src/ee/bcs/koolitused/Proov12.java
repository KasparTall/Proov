package ee.bcs.koolitused;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class Proov12 {

	public static void main(String[] args) {
		Set<String> nimed = new HashSet<>(); // Set<String> nimed = new TreeSet<>(); ei pane suvalises järjekorras ja ta
												// hoiab siin stringe, mida sorteerib tähestiku järjekorras
		nimed.add("Mati");
		nimed.add("Kati");
		nimed.add("Mari");
		System.out.println(nimed);
		nimed.add("Mati");
		nimed.add("Jüri");
		System.out.println(nimed);
		for (String nimi : nimed) {
			System.out.println(nimi.hashCode()); // hashCode on stringimeetod
		}
		nimed.remove("Mati");
		System.out.println(nimed);
		System.out.println("Kas nimekirjas on Muri? - " + nimed.contains("Muri"));
		System.out.println("Kas nimekirjas on Kati? - " + nimed.contains("Kati"));
		System.out.println("Setis on " + nimed.size() + " elementi");

		Set<String> nimed1 = new TreeSet<>();
		nimed1.add("Mati");
		nimed1.add("Kati");
		nimed1.add("Mari");
		System.out.println(nimed1);
		nimed1.add("Mati");
		nimed1.add("Jüri");
		System.out.println(nimed1);
		for (String nimi : nimed1) {
			System.out.println(nimi.hashCode());
		}
		nimed1.remove("Mati");
		System.out.println(nimed1);
		System.out.println("Kas nimekirjas on Muri? - " + nimed1.contains("Muri"));
		System.out.println("Kas nimekirjas on Kati? - " + nimed1.contains("Kati"));
		System.out.println("Setis on " + nimed1.size() + " elementi");

	}
}
