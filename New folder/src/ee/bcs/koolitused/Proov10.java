package ee.bcs.koolitused;

import java.util.ArrayList; //hoidsid kursorit 8.real ja ootad java.util.ArrayList klikkimist, kuna need ei tule defolt projekti kaasa
import java.util.Arrays;
import java.util.List; //hoidsid kursorit 8.real ja ootad java.util.List klikkimist, kuna need ei tule defolt projekti kaasa

public class Proov10 {

	public static void main(String[] args) {
		List<Integer> arvudeLoetelu = new ArrayList<>(); // praegu on tühi kast ()-konstruktor vastutab selle eest
															// midagi luuakse
		arvudeLoetelu.add(7);
		arvudeLoetelu.add(15);
		arvudeLoetelu.add(7);
		arvudeLoetelu.add(-25);
		arvudeLoetelu.add(3678);
		arvudeLoetelu.addAll(Arrays.asList(12, 18, -325));

		System.out.println(arvudeLoetelu);
		arvudeLoetelu.add(3, 92);
		System.out.println(arvudeLoetelu);
		arvudeLoetelu.addAll(1, Arrays.asList(12, 18, -325));
		System.out.println(arvudeLoetelu);

		System.out.println("Listi suurus on: " + arvudeLoetelu.size());

		arvudeLoetelu.remove(2);
		System.out.println(arvudeLoetelu);
		System.out.println("Listi suurus on: " + arvudeLoetelu.size());

		arvudeLoetelu.removeAll(Arrays.asList(7, 92)); // remove.All tahab kollektsiooni sisendiks
		System.out.println(arvudeLoetelu);
		System.out.println("Listi suurus on: " + arvudeLoetelu.size());

		arvudeLoetelu.set(3, 2); // hakkad nullist lugema
		System.out.println(arvudeLoetelu);
		System.out.println("Listi suurus on: " + arvudeLoetelu.size());

		// tee for tsükkel, mille abil saad sa leida listi arvude summa, selleks on
		// get-i vaja
		int summa = 0;
		// siia summa arvudest arvutamine
		for (int i = 0; i < arvudeLoetelu.size(); i++) {
			summa += arvudeLoetelu.get(i);
		}
		System.out.println("Listi olevate arvude summa on : " + summa);

		summa = 0;
		for (Integer arv: arvudeLoetelu) { //for each-iga käib ise listi läbi
			summa += arv;
		}
		System.out.println("Listi olevate arvude summa on : " + summa);
	}

}