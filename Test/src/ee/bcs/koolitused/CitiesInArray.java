package ee.bcs.koolitused;

import java.util.Arrays;

public class CitiesInArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String[][] linnad = { { "Helsinki", "Helsingi", "Helsinki", "Soome" },
				{ "Tallinn", "Tallinn", "Tallinn", "Eesti" }, { "Moscow", "Moskva", "Maskve", "Venemaa" },
				{ "Riga", "Riia", "Rīga", "Läti" }, { "Vilnius", "Vilnius", "Vilnius", "Leedu" } };
		System.out.println(Arrays.deepToString(linnad));
		for (int reaNr = 0; reaNr < linnad.length; reaNr++) {
			//System.out.print(linnad[reaNr][1] + " ");

			System.out.print("\n" + " Riik- " + linnad[reaNr][3] + ": pealinn- " + linnad[reaNr][1]
					+ "; inglise keeles- " + linnad[reaNr][0] + ", kohalikus keeles- " + linnad[reaNr][2]);
		}
	}
}